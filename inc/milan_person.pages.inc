<?php


/**
 * @file
 * Pages to peruse author data.
 */

/**
 * A list of all Author entries.
 *
 *
 * @global object $user Information about person logged in.
 * @return string Rendered HTML.
 */
function milan_person_page_list($theletter=NULL) {
  global $user;
  $_SESSION['person_breadcrumb']= array(l(t('Home') , NULL) , l(t('Milan') , 'milan'));

  $output = '';
  $items = array();
  $author_sql = " AND n.nid IN (SELECT tn.nid FROM {term_data} td , {term_hierarchy} th , {term_node} tn WHERE tn.tid=th.tid AND td.tid=th.tid AND td.name='Author' AND th.parent=(select tid FROM {term_data} WHERE name='Book' AND vid=(Select vid from {vocabulary} WHERE name='Seafarer'))) " ;
  $result = db_query("SELECT DISTINCT substr(replace(upper(n.title) , 'THE' , '') , 1 , 1) theLetter FROM {node} n WHERE n.type = 'person' $author_sql ORDER BY theLetter");
  $output .= l('[' . t('ALL') .']' , 'person');
  while ($node = db_fetch_object($result)) {
    $output .= l('[' . $node->theLetter . ']' , 'person/'. $node->theLetter);
    $has_posts = TRUE;
  }


  if (user_access('create person entry')) {
    $items[] = l(t('Create new person entry.') , "node/add/person");
    $output .= theme('item_list' , $items);
  }

  if ($theletter) {
    $result = db_query("SELECT n.nid FROM {node} n WHERE n.type = 'person' $author_sql AND substr(replace(upper(n.title) , 'THE' , '') , 1 , 1) = '%s' ORDER BY n.title , n.sticky DESC , n.created DESC" , $theletter);
  }
  else {
    $result = db_query("SELECT n.nid FROM {node} n WHERE n.type = 'person' $author_sql ORDER BY n.title , n.sticky DESC , n.created DESC");
  }

  $has_posts = FALSE;

  while ($record = db_fetch_object($result)) {
    $loaded_node = node_load($record->nid);
    $output .= node_view($loaded_node , TRUE , FALSE , TRUE);
    $has_posts = TRUE;
  }

  if ($has_posts) {

  }
  else {
    drupal_set_message(t('No person entries have been created.'));
  }
  return $output;
}

/**
 * Generates javascript to hide layer and reload the ajax
 * select list without reloading the whole form.
 *
 * @return string Javascript
 */
function milan_get_person_ajax_close() {
  $output = 'hideForm(theType); reloadSelect("author");';
  return $output;
}

/**
 *
 * @return string Rendered HTML Form
 */
function milan_get_person_form() {
  return drupal_get_form('milan_make_person_form');
}

/**
 * Implements hook_form()
 */
function milan_make_person_form(&$node) {

  $form = person_make_form($node);
  $form['buttons']['close'] = array(
      '#type' => 'button' ,
      '#value' => t("Close") ,
      '#weight' => 10
  );
  $form['buttons']['submit'] = array(
      '#type' => 'submit' ,
      '#value' => t('Save') ,
      '#weight' => 0
  );
  return $form;
}


/**
 * Implements hook_form_submit()
 */
function milan_make_person_form_submit($form , &$form_state) {
  global $user;
  $result = db_query("SELECT td.tid FROM {term_data} td , {term_hierarchy} th WHERE td.tid=th.tid AND td.name='Author' AND th.parent=(select tid FROM {term_data} WHERE name='Book' AND vid=(Select vid from {vocabulary} WHERE name='Seafarer'))");
  $the_record = db_fetch_object($result);

  person_make_title($form_state);
  $node = node_submit($form_state['values']);
  $node->taxonomy[$the_record->tid]=taxonomy_get_term($the_record->tid);

  $node->name = isset($user->name) ? $user->name : '';
  $node->type = 'person';
  $node->taxonomy=array($the_record->tid);
  node_save($node);
  $thepath='milan/person/add/personajaxclose';
  $form['#redirect'] = $thepath;
  $form_state['redirect'] = $thepath;
}
