<?php


/**
 * @file
 * Pages to peruse world data.
 */

/**
 * A list of all World entries.
 *
 *
 * @global object $user Information about person logged in.
 * @return string Rendered HTML.
 */
function milan_world_list() {
  global $user;
  drupal_add_css(drupal_get_path('module' , 'milan') .'/milan_list.css' , 'module' , 'screen' , FALSE);


  $output = '';
  $items = array();

  if (user_access('administer milan')) {
    $items[] = l(t('Create new book world.') , "milan/world/menuadd");
  }

  $output = theme('item_list' , $items);

  $result = db_query("SELECT td.tid , td.name FROM {term_data} td , {term_hierarchy} th WHERE td.tid=th.tid AND th.parent=(SELECT td.tid FROM {term_data} td , {term_hierarchy} th WHERE td.tid=th.tid AND td.name='World' AND th.parent=(SELECT tid FROM {term_data} WHERE name='Book' AND vid=(SELECT vid FROM {vocabulary} WHERE name='Seafarer'))) ORDER BY td.name");

  $has_posts = FALSE;

  while ($item = db_fetch_object($result)) {
    $output .= l(t($item->name) , 'milan/world/edit/' . $item->tid) . '<br/>';
    $has_posts = TRUE;
  }

  if ($has_posts) {

  }
  else {
    drupal_set_message(t('No book world has been created.'));
  }
  return $output;
}

/**
 * Build a form to edit a World term.
 *
 * @param number $tid A term id.
 * @return string Rendered HTML form.
 */
function milan_edit_world($tid) {
  return drupal_get_form('milan_world_form' , $tid , 'milan/world');
}

/**
 * Build a form to add a World term
 *
 * @return Rendered HTML form.
 */
function milan_add_world() {
  $redirect = 'milan/world';
  return drupal_get_form('milan_world_form' , NULL , $redirect);
}

/**
 * Build a form for an AJAX call to add a World term
 *
 * @return Rendered HTML form.
 */
function milan_ajax_add_world() {
  $redirect = 'milan/world/add/worldajaxclose';
  return drupal_get_form('milan_world_form' , NULL , $redirect);
}

/**
 * Build a form definition for a World term.
 *
 * @param array $form_state The form state.
 * @param number $tid The genre term id.
 * @param string $redirect  Where to redirect after form submission.
 * @return array Form definition
 * Implements drupal_get_form()
 */
function milan_world_form($form_state , $tid=0 , $redirect=NULL) {

  $result = db_query("SELECT * FROM {term_data} WHERE tid=%d" , $tid);
  $my_term = db_fetch_object($result);

  $form = array();

  $form['tid'] = array(
    '#type' => 'hidden' ,
    '#value' => $tid
  );

  $form['name'] = array(
    '#type' => 'textfield' ,
    '#title' => t('Please enter the world name') ,
    '#value' => $my_term->name
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit' ,
    '#value' => t('Save')
  );

  if (strpos($redirect , 'ajax'))  {
    $form['buttons']['close'] = array(
        '#type' => 'button' ,
        '#value' => t("Close") ,
        '#weight' => 10
    );
  }

  if ($my_term) {
    $form['buttons']['delete'] = array(
      '#type' => 'submit' ,
      '#value' => t("Delete")
    );
  }

  $form["#redirect"] = array($redirect , NULL , NULL);


  return $form;
}

/**
 *
 * Implements hook_form_validate()
 */
function milan_world_form_validate($form_id , &$form_state) {
  $name = $form_state['clicked_button']['#post']['name'];
  if (!$name) {
    form_set_error('name' , t('You must enter a world name.'));
  }
}

/**
 *
 * Implements hook_form_submit()
 */
function milan_world_form_submit($form_id , &$form_state) {
  if ($form_state['clicked_button']['#post']['op'] == "Delete") {
    taxonomy_del_term($form_state['clicked_button']['#post']['tid']);
  }
  else {

    $result = db_query("SELECT vid FROM {vocabulary} WHERE name='Seafarer'");
    $my_vid = db_fetch_object($result);

    $form_state['clicked_button']['#post']['vid']=$my_vid->vid;

    $result = db_query("SELECT td.tid FROM {term_data} td , {term_hierarchy} th WHERE td.tid=th.tid AND td.name='World' AND th.parent=(SELECT tid FROM {term_data} WHERE name='Book' AND vid=%d) ORDER BY td.name" , $my_vid->vid);
    $my_parent =db_fetch_object($result);

    $form_state['clicked_button']['#post']['parent']=$my_parent->tid;
    $form_state['redirect']=$form['#redirect'];
    taxonomy_save_term($form_state['clicked_button']['#post']);

  }
}

/**
 * Generates javascript to hide layer and reload the ajax
 * select list without reloading the whole form.
 *
 * @return string Javascript
 */
function milan_get_world_ajax_close() {
  $output = 'hideForm(theType); reloadSelect(theType);';
  return $output;
}