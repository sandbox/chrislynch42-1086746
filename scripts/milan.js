

Drupal.behaviors.milan = function() {

/*  $('<a href="darkstar" class="ajax-milan" id="ajax-milan-person">Add Author</a>').insertBefore('form#node-form');
  $('<span class="ajax-milan">&nbsp;|&nbsp;</span>').insertBefore('form#node-form');
  $('<a href="darkstar" class="ajax-milan" id="ajax-milan-company">Add Publisher</a>').insertBefore('form#node-form');
  $('<span class="ajax-milan">&nbsp;|&nbsp;</span>').insertBefore('form#node-form');
  $('<a href="darkstar" class="ajax-milan" id="ajax-milan-genre">Add Genre</a>').insertBefore('form#node-form');
  $('<span class="ajax-milan">&nbsp;|&nbsp;</span>').insertBefore('form#node-form');
  $('<a href="darkstar" class="ajax-milan" id="ajax-milan-series">Add Series</a>').insertBefore('form#node-form');
  $('<span class="ajax-milan">&nbsp;|&nbsp;</span>').insertBefore('form#node-form');
  $('<a href="darkstar" class="ajax-milan" id="ajax-milan-world">Add World</a>').insertBefore('form#node-form');
  $('<span class="ajax-milan">&nbsp;|&nbsp;</span>').insertBefore('form#node-form');
  $('<a href="darkstar" class="ajax-milan" id="ajax-milan-googlebook">Add Via Google</a>').insertBefore('form#node-form');
*/

  buildLinks('person','Add Author');
  buildLinks('company','Add Publisher');
  buildLinks('genre','Add Genre');
  buildLinks('series','Add Series');
  buildLinks('world','Add World');
  buildLinks('googlebook','Add Via Google');


  bindLinks('person');
  bindLinks('company');
  bindLinks('genre');
  bindLinks('series');
  bindLinks('world');
  bindLinks('googlebook');
}

function buildLinks(theType, theTitle) {
  if($('#ajax-milan-' + theType).size() < 1) {
    $('<span class="ajax-milan"><a href="darkstar" class="ajax-milan" id="ajax-milan-' + theType + '">' + theTitle + '</a>&#160; | &#160;</span>').insertBefore('form#node-form');
   // $('<span>&#160; | &#160;</span>').insertBefore('#ajax-milan-' + theType);
  }
}


function bindLinks(theType) {
  if (!$('div#ajax-milan-' + theType + '-select').length) {
    $('div#edit-' + theType + '-id-wrapper').wrap('<div class="ajax-milan" id="ajax-milan-' + theType + '-select"/>');
  }
  $('a#ajax-milan-' + theType).bind('click' , function(event){
    if (!$('div#ajax-milan-' + theType + '-form').length) {
      $('<div style="position: absolute; top:' + event.pageY + '; left:' + event.pageX + ';" class="ajax-milan ajax-milan-form" id="ajax-milan-' + theType + '-form"></div>').insertBefore('form#node-form');
    }
    $('div#ajax-milan-' + theType + '-form').attr('innerHTML' , '');    
    showForm(theType);
    var theURL = baseURL();
    theURL = theURL + 'milan/' + theType + '/add';

    $.get(theURL, null , function(data) {
      $('div#ajax-milan-' + theType + '-form').attr('innerHTML' , data);
      bindSubmitButton(theType);

      $('div > #edit-close' , $('#ajax-milan-' + theType + '-form')).bind('click' , function(event) {
        hideForm(theType);
        return false;
      });
    });
    return false;
  });
}

function baseURL() {  
  var thePath = window.location.pathname;
  var theURL = window.location.href;
  var pathParts = thePath.split('/');
  if(theURL.indexOf('?q=') > -1) {
    thePath = thePath + '?q=';
  } else {
    pathParts.pop();
    pathParts.pop();
    pathParts.pop();
    thePath = pathParts.join('/') + '/';    
  }
  return thePath;
}

function bindSubmitButton(theType) {
  $('div > #edit-submit' , $('#ajax-milan-' + theType + '-form')).bind('click' , function(event) {
    postString = 'op=Save&' + $('form' , $('div#ajax-milan-' + theType + '-form')).serialize();
    actionString = $('form' , $('div#ajax-milan-' + theType + '-form')).attr('action');
    $.post(actionString , postString , function(data) {
      $('#ajax-milan-' + theType + '-form').attr('innerHTML' , '');
      eval(data);
    });
    return false;
  });
}

function reloadSelect(theType) {
  $('#edit-' + theType + '-id').dblclick();
}

function hideForm(theType) {
  $('div#ajax-milan-' + theType + '-form').hide();
}

function showForm(theType) {
  $('div#ajax-milan-' + theType + '-form').show();
}
