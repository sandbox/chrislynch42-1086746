<?php


/**
 * @file
 * Pages to peruse milan data.
 */

/**
 * List all book entries
 *
 * @global object $user Information about person logged in.
 * @param string $theletter Only list book records beginning with this letter if not null.
 * @return string Themed HTML list.
 */
function milan_page_list($theletter=NULL) {
  global $user;
  drupal_add_js(drupal_get_path('module' , 'milan') . '/milan.js');

  $output = '';
  $items = array();

  if (user_access('edit own milan entries')) {
    $items[] = l(t('Create new book entry.') , "node/add/milan");
  }

  $clean_title = "trim( replace( replace( upper(n.title) , 'THE' , '') ,'.' ,'' ) )";

  $first_letter = 'substr('. $clean_title . ' , 1 , 1)';

  //Create list of letters across top
  $result = db_query("SELECT DISTINCT $first_letter theLetter FROM {node} n WHERE n.type = 'milan' AND n.status = 1 ORDER BY theLetter");
  $output .= l('[' . t('ALL') . ']' , 'milan');
  while ($node = db_fetch_object($result)) {
    $output .= l('[' . $node->theLetter . ']' , 'milan/'. $node->theLetter);
    $has_posts = TRUE;
  }


  $output .= theme('item_list' , $items);
  if ($theletter) {
    $result = db_query(db_rewrite_sql("SELECT n.* FROM {node} n WHERE n.type = 'milan' AND n.status = 1 AND  $first_letter = '%s' ORDER BY $clean_title , n.sticky DESC, n.created DESC") , $theletter);
  }
  else {
    $result = db_query(db_rewrite_sql("SELECT n.* FROM {node} n WHERE n.type = 'milan' AND n.status = 1 ORDER BY $clean_title , n.sticky DESC , n.created DESC"));
  }
  $has_posts = FALSE;

  while ($node = db_fetch_object($result)) {
    $output .= node_view(node_load($node->nid) , 1);
    $has_posts = TRUE;
  }

  if ($has_posts) {
    $output .= theme('pager' , NULL , variable_get('default_nodes_main' , 10));
  }
  else {
    drupal_set_message(t('No book entries have been created.'));
  }
  return $output;
}