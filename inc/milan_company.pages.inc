<?php


/**
 * @file
 * Pages to peruse company data.
 */

/**
 * List all companies that have the Publisher taxonomy.
 *
 * @global object $user Information about the person logged in.
 * @param string $theletter List only those companies beginning with this letter unless it is null.
 * @return string Rendered HTML
 */
function milan_company_page_list($theletter=NULL) {
  global $user;
  $_SESSION['company_breadcrumb']= array(l(t('Home') , NULL) , l(t('Milan') , 'milan'));

  $output = '';
  $items = array();

  if (user_access('create company entry')) {
    $items[] = l(t('Create new company entry.') , "node/add/company");
  }

  $clean_title = "trim( replace( replace( upper(n.title) , 'THE' , '') ,'.' ,'' ) )";

  $first_letter = 'substr('. $clean_title . ' , 1 , 1)';

  $output = theme('item_list' , $items);

  $publisher_sql = " AND n.nid IN (SELECT tn.nid FROM {term_data} td , {term_hierarchy} th , {term_node} tn WHERE tn.tid=th.tid AND td.tid=th.tid AND td.name='Publisher' AND th.parent=(select tid FROM {term_data} WHERE name='Book' AND vid=(Select vid from {vocabulary} WHERE name='Seafarer'))) " ;


  $result = db_query("SELECT DISTINCT $first_letter theLetter FROM {node} n WHERE n.type = 'company' AND n.status = 1 $publisher_sql ORDER BY theLetter");
  $output .= l('[' . t('ALL') . ']' , 'company');
  while ($node = db_fetch_object($result)) {
    $output .= l('[' . $node->theLetter . ']' , 'company/'. $node->theLetter);
    $has_posts = TRUE;
  }

  if ($theletter) {
//    $result = db_query(db_rewrite_sql("SELECT n.nid FROM {node} n WHERE n.type = 'company' AND n.status = 1 AND substr(replace(upper(n.title) , 'THE' , '') , 1 , 1)='%s' $publisher_sql ORDER BY replace(upper(n.title) , 'THE' , ''), n.created DESC") , $theletter);
    $result = db_query(db_rewrite_sql("SELECT n.nid FROM {node} n WHERE n.type = 'company' AND n.status = 1 AND $first_letter='%s' $publisher_sql ORDER BY $clean_title, n.created DESC") , $theletter);
  }
  else {
    $result = db_query(db_rewrite_sql("SELECT n.nid FROM {node} n WHERE n.type = 'company' AND n.status = 1 $publisher_sql ORDER BY $clean_title, n.created DESC"));
  }
  $has_posts = FALSE;

  while ($record = db_fetch_object($result)) {
    $loaded_node = node_load($record->nid);
    $output .= node_view($loaded_node , TRUE , FALSE , TRUE);
    $has_posts = TRUE;
  }

  if ($has_posts) {
    //nothing
  }
  else {
    drupal_set_message(t('No company entries have been created.'));
  }
  return $output;
}

/**
 *  Hide the layer created by AJAX call and reload the publisher
 * select box.
 *
 * @return string Javascript to peform the above.
 */
function milan_get_company_ajax_close() {
  $output = 'hideForm(theType); reloadSelect("publisher");';
  return $output;
}

/**
 * Build company node form for AJAX call.
 *
 *  * @return string Rendered HTML form.
 */
function milan_get_company_form() {
  return drupal_get_form('milan_make_company_form');
}

/**
 * Build a company form.  Uses functions defined  in company.form.inc in
 * company module.
 *
 * @param object $node The node object to be edited if not empty.
 * @return array  Returns array defining form to be rendered.
 * Implements company_make_form()
 */
function milan_make_company_form(&$node) {


  $form = company_make_form($node);

  $form['company']['#title'] = 'Publisher';

  $form['buttons']['close'] = array(
    '#type' => 'button' ,
    '#value' => t("Close") ,
    '#weight' => 10
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit' ,
    '#value' => t('Save') ,
    '#weight' => 0

  );

  return $form;
}

/**
 *
 * Implements hook_form_submit()
 */
function milan_make_company_form_submit($form , &$form_state) {
  global $user;
  $result = db_query("SELECT td.tid FROM {term_data} td , {term_hierarchy} th WHERE td.tid=th.tid AND td.name='Publisher' AND th.parent=(select tid FROM {term_data} WHERE name='Book' AND vid=(Select vid from {vocabulary} WHERE name='Seafarer'))");
  $the_record = db_fetch_object($result);

  $node = node_submit($form_state['values']);
  $node->taxonomy[$the_record->tid]=taxonomy_get_term($the_record->tid);

  $node->name = isset($user->name) ? $user->name : '';
  $node->type = 'company';
  node_save($node);
  $thepath='milan/company/add/companyajaxclose';
  $form['#redirect'] = $thepath;
  $form_state['redirect'] = $thepath;
}
