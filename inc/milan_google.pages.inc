<?php


/**
 * @file
 * Pages to add google data.
 */

/**
 * Build form to accept ISBNs
 *
 * @return Rendered HTML form
 */
function milan_add_googlebook() {
  $redirect = 'milan';
  return drupal_get_form('milan_googlebook_form' , NULL , $redirect);
}

/**
 *
 * Build form to be called via AJAX
 *
 * @return Rendered HTML form
 */
function milan_ajax_add_googlebook() {
  $redirect = 'milan/googlebook/add/googlebookajaxclose';
  return drupal_get_form('milan_googlebook_form' , NULL , $redirect);
}

/**
 * Implements hook_form()
 */
function milan_googlebook_form($form_state , $tid=0 , $redirect=NULL) {
  $form = array();


  $form['isbns'] = array(
    '#type' => 'textarea' ,
    '#title' => t('Please enter the ISBNs') ,
    '#value' => $my_term->name
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit' ,
    '#value' => t('Save')
  );

  $form["#redirect"] = array($redirect , NULL , NULL);


  if (strpos($redirect , 'ajax'))  {
    $form['ajaxcalled'] = array(
      '#type' => 'hidden' ,
      '#value' => 'TRUE'
    );
    $form['buttons']['close'] = array(
        '#type' => 'button' ,
        '#value' => t("Close") ,
        '#weight' => 10
    );
  }

  return $form;
}
/**
 * Implements hook_form_validate()
 */
function milan_googlebook_form_validate($form_id , &$form_state) {
}

/**
 * Implements hook_form_submit()
 */
function milan_googlebook_form_submit(&$form , &$form_state) {
  $search_data = $form_state['clicked_button']['#post']['isbns'];
  $search_array = split(',' , $search_data);
  $url_string = 'http://books.google.com/books/feeds/volumes?q=';
  foreach ($search_array as $search_string) {
    $book_node = milan_googlebook_curl($url_string . milan_googlebook_search_cleanup($search_string));
  }
    if(!$book_node) {
      drupal_set_message("A book was not added.");
    }
    $form_state['redirect'] = $form['#redirect'][0];
    if ($form_state['clicked_button']['#post']['ajaxcalled']) {
      $form['#redirect'][0] = $form['#redirect'][0] . '/' . $book_node->nid;
      $form_state['redirect'] = $form['#redirect'][0] . '/' . $book_node->nid;
    }

}

function milan_googlebook_search_cleanup($search_string) {
  $search_string = trim($search_string);
  $search_string = str_replace("-","",$search_string);

  return $search_string;
}

/**
 * Connect to a server using a URL and retrieve the response.
 *
 * @param string $url URL to connect to and receive data from
 * @return object An object representing a book.
 */
function milan_googlebook_curl($url) {
  $the_request = curl_init($url);
  curl_setopt($the_request , CURLOPT_RETURNTRANSFER , TRUE);
  curl_setopt($the_request , CURLOPT_HEADER , FALSE);
  curl_setopt($the_request , CURLOPT_FOLLOWLOCATION , TRUE);
//  curl_setopt($the_request, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9');  
  $the_response = curl_exec($the_request);

  curl_close($the_request);
  return milan_handle_googlebook_response($the_response);
}

/**
 * Parses string information as XML and pulls book data from the string.
 * @global object $user Data about the person logged in.
 * @param string $the_response Contains XML describing a book(s)
 * @return object A book node
 */
function milan_handle_googlebook_response($the_response) {
  global $user;
  $the_dom = new DOMDocument();
  $the_dom->loadXML($the_response);
  $entries = $the_dom->getElementsByTagName('entry');
  $book_node = NULL;
  foreach ($entries as $entry) {

    $title="";
    $titles = $entry->getElementsByTagName('title');
    for ($i=1;$i < $titles->length;$i++) {
      if (drupal_strlen($titles->item($i)->nodeValue) > 0) {
        if($i > 1) {
          $title = $title . " ";
        }
        $title = $title . $titles->item($i)->nodeValue;
      }
    }
    $book_node = milan_googlebook_get_book($title);
    $book_node->title=$title;

    $book_node->name = $user->name;
    $book_node->type = 'milan';
    $book_node->author_id=array();

    $authors = $entry->getElementsByTagName('creator');
    $author_nodes = array();
    $publisher_node=NULL;
    $counter =0;
    $suffix_array = array('SR.' , 'JR.' , 'II' , 'III' , 'IV' , 'V');
    for ($i=0;$i < $authors->length;$i++) {
      $the_suffix = NULL;
      if (in_array(drupal_strtoupper($authors->item($i)->nodeValue) , $suffix_array)) {
        //nothing
      }
      else {
        if (($i + 1) < $authors->length && in_array(drupal_strtoupper($authors->item($i+1)->nodeValue) , $suffix_array)) {
          $the_suffix = $authors->item($i+1)->nodeValue;
        }
        $author_nodes[$counter]=milan_googlebook_add_author($authors->item($i)->nodeValue , $the_suffix);
        $book_node->author_id[$counter]=$author_nodes[$counter]->person_id;
        $counter++;
      }
    }

    $publisher = $entry->getElementsByTagName('publisher');
    for ($i=0;$i < $publisher->length;$i++) {
      if (drupal_strlen($publisher->item($i)->nodeValue) > 0) {
        $publisher_node=milan_googlebook_add_publisher($publisher->item($i)->nodeValue);
        $book_node->publisher_id=$publisher_node->company_id;
      }
    }

    $date = $entry->getElementsByTagName('date');
    if ($date->length) {
      $book_node->year=$date->item(0)->nodeValue;
    }

    $description = $entry->getElementsByTagName('description');
    if ($description->length) {
      $book_node->body=$description->item(0)->nodeValue;
    }


    $ids = $entry->getElementsByTagName('identifier');
    foreach ($ids as $id) {
      if (strstr($id->nodeValue , 'ISBN')) {
        $temp_array = split(':' , $id->nodeValue);
        if (count($temp_array) > 0) {
          $book_node->isbn=trim($temp_array[1]);
        }
        else {
          $book_node->isbn=$id->nodeValue;
        }
      }
    }
    node_save($book_node);
  }


  return $book_node;
}


/**
 * Check to see if the book is already in the system.
 * @param string $the_name The name of the book
 * @return object A book node.
 */
function milan_googlebook_get_book($the_name) {
  $the_args = array('title' => $the_name , 'type' => 'milan');
  $book_node = node_load($the_args);
  if ($book_node && $book_node->nid) {
    return $book_node;
  }
  else {
    return FALSE;
  }
}

/**
 * Check to see if the author is already in the system.
 * @param string $lastname Author's last name.
 * @param string $firstname Author's first name.
 * @param string $middlename Author's middle name.
 * @param string $the_suffix Author's name suffix.  Jr , Sr. , III etc.
 * @return object Person node with Author taxonomy
 */
function milan_googlebook_get_author($lastname=NULL , $firstname=NULL , $middlename=NULL , $the_suffix=NULL) {
  $the_args = array();
  $the_query = "SELECT * FROM {person} WHERE first_name='%s'";
  $the_args[count($the_args)]=$firstname;
  if ($lastname) {
    $the_query .= " AND";
    $the_query .= " last_name='%s'";
    $the_args[count($the_args)]=$lastname;
  }
  if ($middlename) {
    $the_query .= " AND";
    $the_query .= " middle_name='%s'";
    $the_args[count($the_args)]=$middlename;
  }

  if ($the_suffix) {
    $the_query .= " AND";
    $the_query .= " suffix_name='%s'";
    $the_args[count($the_args)]=$the_suffix;
  }
  $result = db_query($the_query , $the_args);

  if ($author = db_fetch_object($result)) {
    return node_load($author->nid);
  }
  else {
    return FALSE;
  }
}

/**
 * Add an author to the system if they don't already exist.
 * @global object $user Individual logged into Drupal
 * @param string $the_name Author's full name
 * @param string $the_suffix Author's name suffix
 * @return object Person node associated with Author term.
 */
function milan_googlebook_add_author($the_name , $the_suffix=NULL) {
  global $user;
  $name_array = split(' ' , $the_name);
  $firstname = $name_array[0];
  if (count($name_array) == 2) {
    $lastname = $name_array[1];
  }
  elseif (count($name_array) == 3) {
    $lastname = $name_array[2];
    $middlename = $name_array[1];
  }
  else {
    $lastname = $name_array[count($name_array) - 1];
    for ($i=1;$i < count($name_array);$i++) {
      $middlename .= $name_array[$i];
    }
  }

  $add_record = TRUE;

  $author_node = milan_googlebook_get_author($lastname , $firstname , $middlename , $the_suffix);

  if ($author_node && $author_node->nid) {
    ////nothing
  }
  else {
    $result = db_query("SELECT td.tid FROM {term_data} td , {term_hierarchy} th WHERE td.tid=th.tid AND td.name='Author' AND th.parent=(select tid FROM {term_data} WHERE name='Book' AND vid=(Select vid from {vocabulary} WHERE name='Seafarer'))");
    $the_record = db_fetch_object($result);

    $author_node = new stdClass();
    $author_node->taxonomy=array();
    $author_node->taxonomy[$the_record->tid]=taxonomy_get_term($the_record->tid);

    $author_node->name = $user->name;
    $author_node->type = 'person';
    $author_node->last_name=$lastname;
    $author_node->first_name=$firstname;
    $author_node->middle_name=$middlename;
    $author_node->suffix_name=$the_suffix;
    $author_node->title = $author_node->last_name;
    if (defined($author_node->suffix_name)) {
      $author_node->title .= ' ' . $author_node->suffix_name;
    }
    $author_node->title .= ' , ' . $author_node->first_name . ' ' . $author_node->middle_name;
    node_save($author_node);
    $author_node = milan_googlebook_get_author($lastname , $firstname , $middlename , $the_suffix);
  }
  return $author_node;
}

/**
 * Check to see if the publisher is already in the system.
 * @param string $the_name Publisher's name.
 * @return object Company node associated with Publisher term
 */
function milan_googlebook_get_publisher($the_name) {
  $the_args = array('title' => $the_name , 'type' => 'company');
  $publisher_node = node_load($the_args);
  if ($publisher_node && $publisher_node->company_id) {
    return $publisher_node;
  }
  else {
    return FALSE;
  }
}

/**
 * Add an publisher to the system if it doesn't already exist.
 * @global object $user Individual logged into Drupal
 * @param string $the_name Publisher's name
 * @return object Company node associated with Publisher term.
 */
function milan_googlebook_add_publisher($the_name) {
  global $user;

  $add_record = TRUE;
  $publisher_node = milan_googlebook_get_publisher($the_name);
  if ($publisher_node && $publisher_node->nid) {
    ////////////nothing
  }
  else {
    $result = db_query("SELECT td.tid FROM {term_data} td , {term_hierarchy} th WHERE td.tid=th.tid AND td.name='Publisher' AND th.parent=(select tid FROM {term_data} WHERE name='Book' AND vid=(Select vid from {vocabulary} WHERE name='Seafarer'))");
    $the_record = db_fetch_object($result);

    $publisher_node = new stdClass();
    $publisher_node->taxonomy=array();
    $publisher_node->taxonomy[$the_record->tid]=taxonomy_get_term($the_record->tid);
    $publisher_node->name = $user->name;
    $publisher_node->type = 'company';
    $publisher_node->title=$the_name;
    node_save($publisher_node);
    $publisher_node = milan_googlebook_get_publisher($the_name);
  }
  return $publisher_node;
}

/**
 * Close ajax window and go to node edit form if book found.
 * @param number $nid Node id of book.
 * @return string Javascript
 */
function milan_get_googlebook_ajax_close($nid=NULL) {

  if ($nid) {
    $output = "window.document.location.href='" . base_path() . 'node/' . $nid . "/edit';";
  }
  else {
    $output = "hideForm(theType);";
  }
  return $output;
}