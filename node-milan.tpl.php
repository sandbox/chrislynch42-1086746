<?php


/**
 * @file
 * Page to display a milan node.
 */
?>
<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php print $picture ?>
  <?php if ($page == 0): ?>
    <h1 class="title"><a href="<?php print $node_url ?>" class="milanNodeTitleLink"><?php print $title ?></a></h1>
  <?php endif; ?>
    <span class="submitted"><?php print $submitted ?></span><br/>
    <table id="holdcontent" class="cleanup">
      <tr>
        <td>
          <span class="content"><?php print $body ?></span>
        </td>
      </tr>
    </table>
    <?php if ($links): ?>
    <span class="links">&raquo; <?php print $links ?></span>
    <?php endif; ?>
</div>
